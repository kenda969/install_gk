#!/bin/bash
cd automake-1.15
./configure
make
make install
cd ../
cd flex-2.6.4
chmod 777 /root/flex-2.6.4/*
./autogen.sh
./configure
make
make install
cd ../
export PTLIBDIR=~/ptlib
mv ptlib-2_10_9_2/ ptlib
cd ptlib
./configure --enable-ipv6 --disable-odbc --disable-sdl --disable-lua --disable-expat --enable-openldap
make optnoshared
cd ../
mv h323plus-1_27_0 h323plus
cd h323plus
export OPENH323DIR=~/h323plus
./configure --enable-h235 --enable-h264 -enable-h46017 --enable-h46026
make optnoshared
cd ../
mv gnugk-5_3 gnugk
cd gnugk/
./configure --enable-h46018 --with-large_fdset=1024
make optnoshared
cd ../
cp /root/gnugk/obj_linux_x86_64_s/* /usr/sbin/gnugk
systemctl start gnugk
systemctl enable gnugk

